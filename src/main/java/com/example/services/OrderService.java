package com.example.services;

import com.example.OrderServiceApplication;
import com.example.converters.SuperConverter;
import com.example.dtos.OrderDto;
import com.example.dtos.StateDto;
import com.example.entities.Order;
import com.example.entities.State;
import com.example.enums.OrderState;
import com.example.jms.JmsSender;
import com.example.repositories.OrderRepository;
import com.example.repositories.StateRepository;
import com.example.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import static com.example.constants.Constant.QUEUE_ORDER_SERVICE_PENDING_ORDER;

import javax.jms.JMSException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class OrderService implements DateUtil {

    @Autowired
    @Qualifier("orderOrderDtoConverter")
    private SuperConverter<Order, OrderDto> converter;

    @Autowired
    @Qualifier("stateStateDtoConverter")
    private SuperConverter<State, StateDto> stateConverter;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private JmsSender<Order> orderJmsSender;

    @Autowired
    private JmsSender<OrderDto> orderDtoJmsSender;

    public OrderDto createPendingOrder(OrderDto orderDto) throws JMSException {

        //create new order
        final Order order1 = new Order();
        order1.setUsername(orderDto.getUsername());
        order1.setCreationDate(getCurrentDate());
        order1.setPriceAmount(orderDto.getPriceAmount());
        order1.setVatRate(orderDto.getVatRate());
        order1.setNetAmount(orderDto.getNetAmount());
        orderRepository.save(order1);

        //save NEW state
        final State newState = stateConverter.toEntity(orderDto.getStates().get(0));
        newState.setOrder(order1);
        stateRepository.save(newState);

        //add PENDING state
        final State pendingState = new State();
        pendingState.setOrderState(OrderState.PENDING);
        pendingState.setEventDate(getCurrentDate());
        pendingState.setOrder(order1);
        //save PENDING state
        stateRepository.save(pendingState);

        //detach order
        newState.setOrder(null);
        pendingState.setOrder(null);

        //add states to order
        final List<State> states = new ArrayList<>();
        states.add(newState);
        states.add(pendingState);
        order1.setStates(states);

        //save order for all instances
        orderJmsSender.pushOnJms(OrderServiceApplication.getSaveOrderTopic(), order1);

        //toDo
        //add insert into outBox with transactional rollbackFor

        //send pending order to user-service
        final OrderDto orderDto1 = converter.toDto(order1);
        orderDtoJmsSender.pushOnJms(QUEUE_ORDER_SERVICE_PENDING_ORDER, orderDto1);

        //return order
        return orderDto1;
    }

    public OrderDto getOrder(final String id){
        final UUID uuid = UUID.fromString(id);
        final Order order = orderRepository.findById(uuid);
        final OrderDto orderDto = converter.toDto(order);
        return orderDto;

    }
}
