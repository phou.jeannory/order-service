package com.example.converters;

import com.example.dtos.OrderDto;
import com.example.entities.Order;
import org.springframework.stereotype.Service;

@Service("orderOrderDtoConverter")
public class OrderOrderDtoConverter<E, D> extends SuperConverter<E, D> {

    public OrderOrderDtoConverter() {
        super(Order.class, OrderDto.class);
    }
}
