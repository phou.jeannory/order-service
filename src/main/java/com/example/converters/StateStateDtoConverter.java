package com.example.converters;

import com.example.dtos.OrderDto;
import com.example.dtos.StateDto;
import com.example.entities.Order;
import com.example.entities.State;
import org.springframework.stereotype.Service;

@Service("stateStateDtoConverter")
public class StateStateDtoConverter<E, D> extends SuperConverter<E, D> {

    public StateStateDtoConverter() {
        super(State.class, StateDto.class);
    }
}
