package com.example.converters;

import com.example.singleton.SingletonBean;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class SuperConverter<E, D> {

    @Getter
    private final Class entityClass;
    @Getter
    private final Class dtoClass;
    @Autowired
    @Getter
    private SingletonBean singletonBean;

    public SuperConverter(Class entityClass, Class dtoClass) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }

    public D toDto(E entity) {
        return getSingletonBean().getModelMapper().map(entity, (Type) dtoClass);
    }

    public E toEntity(D dto) {
        return getSingletonBean().getModelMapper().map(dto, (Type) entityClass);
    }

    public List<D> toDtos(final List<E> entities) {
        return entities.stream().map(this::toDto).collect(Collectors.toList()).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    public List<E> toEntities(final List<D> dtos) {
        return dtos.stream().map(this::toEntity).collect(Collectors.toList()).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

}
