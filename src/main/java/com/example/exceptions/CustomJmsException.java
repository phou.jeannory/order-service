package com.example.exceptions;

public class CustomJmsException extends RuntimeException {

    public CustomJmsException(String message) {
        super(message);
    }
}
