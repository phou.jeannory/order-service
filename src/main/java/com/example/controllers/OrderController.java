package com.example.controllers;

import com.example.dtos.OrderDto;
import com.example.dtos.StateDto;
import com.example.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.jms.JMSException;

@CrossOrigin("*")
@RestController
@RequestMapping("api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * receive new order
     * send pending order to user-service
     * return pending order
     */
    @RequestMapping(value="/create")
    public ResponseEntity<OrderDto> toOrder(@RequestBody OrderDto orderDto) throws JMSException {
        final OrderDto orderDto1 = orderService.createPendingOrder(orderDto);
        return new ResponseEntity<OrderDto>(orderDto1, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * receive new order
     * send pending order to user-service
     * return pending order
     */
    @RequestMapping
    public ResponseEntity<OrderDto> toOrder(@RequestParam String id) throws JMSException {
        final OrderDto orderDto1 = orderService.getOrder(id);
        return new ResponseEntity<OrderDto>(orderDto1, new HttpHeaders(), HttpStatus.OK);
    }
}
