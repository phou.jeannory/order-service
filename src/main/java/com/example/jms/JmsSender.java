package com.example.jms;

import com.example.exceptions.CustomJmsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Topic;

@Component
public class JmsSender <T> {

    @Autowired
    private JmsTemplate jmsTemplate;

    public T pushOnJms(final Topic topic, final T t) throws JMSException {
        try {
            jmsTemplate.convertAndSend(topic, t);
            jmsTemplate.setReceiveTimeout(10_000);
            return t;
        }catch (Exception ex){
            throw new CustomJmsException("Message cannot be sent : " + ex.getMessage());
        }
    }

    public T pushOnJms(final String queue, final T t) throws JMSException {
        try {
            jmsTemplate.convertAndSend(queue, t);
            jmsTemplate.setReceiveTimeout(10_000);
            return t;
        }catch (Exception ex){
            throw new CustomJmsException("Message cannot be sent : " + ex.getMessage());
        }
    }

}
