package com.example.jms;

import com.example.converters.SuperConverter;
import com.example.dtos.OrderDto;
import com.example.entities.Order;
import com.example.repositories.OrderRepository;
import com.example.repositories.StateRepository;
import com.google.gson.Gson;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.listener.adapter.JmsResponse;
import org.springframework.stereotype.Component;
import javax.jms.JMSException;
import javax.jms.Message;

import static com.example.constants.Constant.*;

@Component
@EnableJms
public class JmsReceiver {

    @Autowired
    private Gson  gson;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    @Qualifier("orderOrderDtoConverter")
    private SuperConverter<Order, OrderDto> converter;

    //save order for all instances
    @org.springframework.jms.annotation.JmsListener(destination = TOPIC_ORDER_SERVICE_SAVE_ORDER, containerFactory = "userFactory")
    public JmsResponse saveOrder (Message message) throws JMSException {
        ActiveMQTextMessage amqMessage = (ActiveMQTextMessage) message;
        final String messageResponse = amqMessage.getText();
        final Order order = gson.fromJson(messageResponse, Order.class);
        System.out.println("saveOrder for this instance ");
        orderRepository.save(order);
        order.getStates().forEach(state->{
            System.out.println("save states for this instance ");
            state.setOrder(order);
            stateRepository.save(state);
        });
        return null;
    }


    @org.springframework.jms.annotation.JmsListener(destination = TOPIC_USER_SERVICE_FINALIZE_ORDER, containerFactory = "userFactory")
    public JmsResponse finalizeOrder (Message message) throws JMSException {
        ActiveMQTextMessage amqMessage = (ActiveMQTextMessage) message;
        final String messageResponse = amqMessage.getText();
        final OrderDto orderDto = gson.fromJson(messageResponse, OrderDto.class);
        System.out.println("finalizeOrder for this instance ");
        final Order order = converter.toEntity(orderDto);
        order.toString();
        orderRepository.save(order);
        order.getStates().forEach(state->{
            System.out.println("save states for this instance ");
            state.setOrder(order);
            stateRepository.save(state);
        });
        return null;
    }



}
