package com.example.entities;

import com.example.enums.OrderState;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Table(name = "state_app")
@Entity
public class State {

    @Getter @Setter
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;
    @Getter @Setter
    private String eventDate;
    @Getter @Setter
    @Enumerated(EnumType.STRING)
    private OrderState orderState;
    @Setter
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;
}
