package com.example.entities;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Table(name = "order_app")
@Entity
public class Order {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    @Getter @Setter
    private UUID id;
    @Getter @Setter
    @Column(nullable = false)
    private String username;
    @Getter @Setter
    private String creationDate;
    @Getter @Setter
    private int priceAmount;
    @Getter @Setter
    private int vatRate;
    @Getter @Setter
    private int netAmount;
    @Getter
    @Setter
    @OneToMany(mappedBy = "order")
    private List<State> states;
}


