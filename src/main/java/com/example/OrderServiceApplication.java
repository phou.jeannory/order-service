package com.example;

import com.example.entities.Order;
import com.example.entities.State;
import com.example.enums.OrderState;
import com.example.repositories.OrderRepository;
import com.example.repositories.StateRepository;
import lombok.Getter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.JMSException;
import javax.jms.Topic;

import static com.example.constants.Constant.TOPIC_ORDER_SERVICE_SAVE_ORDER;
import static com.example.constants.Constant.TOPIC_USER_SERVICE_FINALIZE_ORDER;


@EnableDiscoveryClient
@EnableConfigurationProperties
@SpringBootApplication
public class OrderServiceApplication {

    @Getter
    private static Topic saveOrderTopic;

    @Getter
    private static Topic finalizeOrderTopic;

    public static void main(String[] args) throws JMSException {
        final ConfigurableApplicationContext context = SpringApplication.run(OrderServiceApplication.class, args);
        final JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
        saveOrderTopic = jmsTemplate.getConnectionFactory().createConnection()
                .createSession().createTopic(TOPIC_ORDER_SERVICE_SAVE_ORDER);
        finalizeOrderTopic = jmsTemplate.getConnectionFactory().createConnection()
                .createSession().createTopic(TOPIC_USER_SERVICE_FINALIZE_ORDER);
    }

    @Bean
    CommandLineRunner start(
            final OrderRepository orderRepository,
            final StateRepository stateRepository
    ) {
        return arg -> {
            final Order order1 = new Order();
            order1.setUsername("john.doe@gmail.com");
            order1.setCreationDate("2020-01-01 à 10h05");
            order1.setPriceAmount(636); //in cents
            order1.setVatRate(100); //10%
            order1.setNetAmount(700);
            orderRepository.save(order1);

            final State state1 = new State();
            state1.setEventDate("2020-01-01 à 10h10");
            state1.setOrderState(OrderState.PENDING);
            state1.setOrder(order1);

            final State state2 = new State();
            state2.setEventDate("2020-01-01 à 10h15");
            state2.setOrderState(OrderState.REJECT);
            state2.setOrder(order1);

            stateRepository.save(state1);
            stateRepository.save(state2);

            final Order order2 = new Order();
            order2.setUsername("john.doe@gmail.com");
            order2.setCreationDate("2020-01-01 à 11h05");
            order2.setPriceAmount(636); //in cents
            order2.setVatRate(100); //10%
            order2.setNetAmount(700);
            orderRepository.save(order2);

            final State state3 = new State();
            state3.setEventDate("2020-01-01 à 11h10");
            state3.setOrderState(OrderState.PENDING);
            state3.setOrder(order2);

            final State state4 = new State();
            state4.setEventDate("2020-01-01 à 11h15");
            state4.setOrderState(OrderState.APPROVE);
            state4.setOrder(order2);

            stateRepository.save(state3);
            stateRepository.save(state4);

        };
    }

}
