package com.example.enums;

public enum OrderState {
    NEW,
    PENDING,
    APPROVE,
    REJECT,
    ERROR
}
