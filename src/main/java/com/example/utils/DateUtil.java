package com.example.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    default String getCurrentDate(){
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.format(new Date());
    }
}
