package com.example.dtos;

import com.example.enums.OrderState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StateDto {

    private UUID id;
    private String eventDate;
    private OrderState orderState;

}
