package com.example.dtos;

import lombok.*;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private UUID id;
    private String username;
    private String creationDate;
    private int priceAmount;
    private int vatRate;
    private int netAmount;

    private List<StateDto> states;
}
