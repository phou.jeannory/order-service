package com.example.constants;

public class Constant {

    public static final String TOPIC_ORDER_SERVICE_SAVE_ORDER = "topic.order-service.save-order";

    public final static String TOPIC_USER_SERVICE_FINALIZE_ORDER = "topic.user-service.finalize-order";

    public final static String QUEUE_ORDER_SERVICE_PENDING_ORDER = "queue.order-service.pending-order";

}
