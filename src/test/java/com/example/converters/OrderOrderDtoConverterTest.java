package com.example.converters;

import com.example.dtos.OrderDto;
import com.example.dtos.StateDto;
import com.example.entities.Order;
import com.example.entities.State;
import com.example.enums.OrderState;
import com.example.singleton.SingletonBean;
import com.example.utils.DateUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class OrderOrderDtoConverterTest implements DateUtil {

    @InjectMocks
    private SuperConverter<Order, OrderDto> converter;

    @Mock
    private SingletonBean singletonBean;

    @Before
    public void setUp() throws Exception {
        this.converter = new OrderOrderDtoConverter<>();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void toDto() throws InterruptedException {
        //given
        final UUID orderId = UUID.randomUUID();
        final UUID stateOrderId = UUID.randomUUID();
        final Order order = Mockito.spy(new Order());
        order.setId(orderId);
        order.setUsername("user1");
        order.setCreationDate(getCurrentDate());
        order.setPriceAmount(636); //in cents
        order.setVatRate(100); //10%
        order.setNetAmount(700);
        Thread.sleep(1000);
        final  State state = new State();
        state.setId(stateOrderId);
        state.setEventDate(getCurrentDate());
        state.setOrderState(OrderState.NEW);
        final List<State> states = new ArrayList<>();
        states.add(state);
        Mockito.when(order.getStates()).thenReturn(states);
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final OrderDto result = converter.toDto(order);

        //then
        Assert.assertEquals(orderId, result.getId());
        Assert.assertEquals(700, result.getNetAmount());
        Assert.assertEquals(1, result.getStates().size());
        Assert.assertEquals(stateOrderId, result.getStates().get(0).getId());
    }

    @Test
    public void toEntity() throws InterruptedException {
        //given
        final UUID orderId = UUID.randomUUID();
        final UUID stateOrderId = UUID.randomUUID();
        final OrderDto orderDto = new OrderDto();
        orderDto.setId(orderId);
        orderDto.setUsername("user1");
        orderDto.setCreationDate(getCurrentDate());
        orderDto.setPriceAmount(636); //in cents
        orderDto.setVatRate(100); //10%
        orderDto.setNetAmount(700);
        Thread.sleep(1000);
        final StateDto stateDto = new StateDto();
        stateDto.setId(stateOrderId);
        stateDto.setEventDate(getCurrentDate());
        stateDto.setOrderState(OrderState.NEW);
        orderDto.setStates(Collections.singletonList(stateDto));
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final Order result = converter.toEntity(orderDto);

        //then
        Assert.assertEquals(orderId, result.getId());
        Assert.assertEquals(700, result.getNetAmount());
        //lose states with conversion
        Assert.assertEquals(1, result.getStates().size());
        Assert.assertEquals(stateOrderId, result.getStates().get(0).getId());
    }
}